### unity-voxel-drawcalls

Notes on draw calls and vertices in Unity.

The scene includes input based on keys to spawn instances of test cubes 

* View in Play window with Stats on to see draw call batching

* Press keys Y, U, I to spawn first row, and H, J, K, to spawn second row. 

* Tap key a second time to change color, observe draw calls.

#### Vertices

Skybox generates ~20k vertices.

#### Draw Calls

With one cube prefab, one instance and one material requires 1 draw call.

Two instances are two draw calls but are then batched if GPU instancing used.

#### Colors

Material vs. sharedMaterial: 

* modifying material creates a new instance, will always create a new draw call.

* Need a material instance for each color.

MaterialPropertyBlock allows multiple colors for one material, with no additional draw calls.

* See implementation in SpawnTester.cs