﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTester : MonoBehaviour
{
    public GameObject testCube;

    private GameObject[] cubeRow1;
    private GameObject[] cubeRow2;

    private MaterialPropertyBlock sharedProps;

    void Awake() {
        cubeRow1 = new GameObject[3];
        cubeRow2 = new GameObject[3];
    }

    void SpawnCube(GameObject[] rowTarget, int index, GameObject cubePrefab, Vector3 pos) {
        rowTarget[index] = cubePrefab.CreateGameObject(pos, Quaternion.identity, false);
    }

    void ColorCube(GameObject rowTarget, Color toColor) {

        if (sharedProps == null) {
            sharedProps = new MaterialPropertyBlock();
        }
        MeshRenderer renderer;

        sharedProps.SetColor("_Color", toColor);

        renderer = rowTarget.GetComponent<MeshRenderer>();
        renderer.SetPropertyBlock(sharedProps);

    }

    void ModifyCube(GameObject[] rowTarget, int index, GameObject cubePrefab, Vector3 pos, Color toColor) {
        if (rowTarget[index] == null) {
            SpawnCube(rowTarget, index, cubePrefab, pos);
        }
        else {
            ColorCube(rowTarget[index], toColor);
        }
    }

    void Update()
    {
        // Input for testing draw calls

        // test cube 1 spawn
        if (Input.GetKeyDown(KeyCode.Y)) {
            ModifyCube(cubeRow1, 0, testCube, Vector3.zero, Color.blue);
        }
        else if (Input.GetKeyDown(KeyCode.U)) {
            ModifyCube(cubeRow1, 1, testCube, Vector3.zero.WithX(1), Color.blue);
        }
        else if (Input.GetKeyDown(KeyCode.I)) {
            ModifyCube(cubeRow1, 2, testCube, Vector3.zero.WithX(2), Color.blue);
        }

        // test cube 2 spawn
        if (Input.GetKeyDown(KeyCode.H)) {
            ModifyCube(cubeRow2, 0, testCube, Vector3.zero.WithY(1.5f), Color.green);
        }
        else if (Input.GetKeyDown(KeyCode.J)) {
            ModifyCube(cubeRow2, 1, testCube, Vector3.zero.WithY(1.5f).WithX(1), Color.green);
        }
        else if (Input.GetKeyDown(KeyCode.K)) {
            ModifyCube(cubeRow2, 2, testCube, Vector3.zero.WithY(1.5f).WithX(2), Color.green);
        }
    }
}
