﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class ObjectPoolerItem {
    public GameObject prefabToPool;
    public int amountToPool;
    public bool shouldExpand;
}


// pools all objects, make sure every item is a prefab
public class ObjectPooler : MonoBehaviour {

    public static ObjectPooler SharedInstance;

    public List<GameObject> pooledObjects;

    public List<ObjectPoolerItem> itemsToPool;

    private void Awake() {
        SharedInstance = this;
    }

    private void Start() {
        pooledObjects = new List<GameObject>();
        foreach (ObjectPoolerItem item in itemsToPool) {
            for (int i = 0; i < item.amountToPool; i++) {
                GameObject obj = (GameObject)Instantiate(item.prefabToPool);
                obj.Hide();
                pooledObjects.Add(obj);
            }
        }
    }

    public GameObject GetPooledObject(string name) {

        for (int i = 0; i < pooledObjects.Count; i++) {
            if (!pooledObjects[i].activeInHierarchy && pooledObjects[i].name.Contains(name)) {
                Debug.Log("\npooledObjectsname: " + pooledObjects[i].name + "\ngo.name: " + name + "\n\n\n");
                return pooledObjects[i];
            }
        }
        foreach (ObjectPoolerItem item in itemsToPool) {
            if (item.shouldExpand) {
                GameObject obj = (GameObject)Instantiate(item.prefabToPool);
                obj.Hide();
                pooledObjects.Add(obj);
                return obj;
            }
        }
        return null;
    }


}