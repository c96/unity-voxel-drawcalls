﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class FormatUtility {

    public static string ConvertStringToBase64(string val) {

        byte[] bytes = System.Text.Encoding.UTF8.GetBytes(val);
        return Convert.ToBase64String(bytes);
    }

    public static string ConvertBase64ToString(string val) {

        byte[] bytes = Convert.FromBase64String(val);
        return System.Text.Encoding.UTF8.GetString(bytes);
    }

}